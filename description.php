
<div class="flex-container-description">
  <div class="showhim">OS Support
    <br/>
    <i class="fa fa-caret-down"></i>
    <div class="showme">IOS 8.0+ dan Android 4.4+</div>
  </div>
  <div class="showhim">Dimentions
    <br/>
    <i class="fa fa-caret-down"></i>
    <div class="showme">1.3″ inch</div>
  </div>
  <div class="showhim">Water Resistence
    <br/>
    <i class="fa fa-caret-down"></i>
    <div class="showme">5 ATM</div>
  </div>
  <div class="showhim">Strap Size
    <br/>
    <i class="fa fa-caret-down"></i>
    <div class="showme">240 x 16 x 11.0 mm+</div>
  </div>
  <div class="showhim">Battery Capacity
    <br/>
    <i class="fa fa-caret-down"></i>
    <div class="showme">210 mAh Battery</div>
  </div>
</div>
