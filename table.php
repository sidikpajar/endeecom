<div style="margin-bottom:50px">
    <p class="title-custom">Fitur Utama Start Go S1</p>
    <div class="centering">
      <table class="aligncenter" style="border-collapse: collapse; width: 85%;" border="1">
          <tbody>
              <tr>
                  <td id="imagewidth">
                      <center>
                          <img
                              class="wp-image-192 aligncenter tc-smart-load-skip czr-smart-loaded"
                              src="http://startgo-id.com/new/wp-content/uploads/2019/10/icon-aer-300x300.png"
                              alt=""
                              width="150"
                              height="150"
                              style="display: block;"
                          />
                      </center>
                  </td>
                  <td id="contentwidth">
                      <strong>Waterproof</strong><br />
                      Tetap mendapatkan notofikasi ketika kamu sedang di dalam air bahkan sampai 50 Meter kedalaman, sekarang dapat menjadi kenyataan! Karena Start Go S1, sudah dilengkapi dengan fitur waterproof.
                  </td>
              </tr>
              <tr>
                  <td id="imagewidth">
                      <center>
                          <img
                              src="http://startgo-id.com/new/wp-content/uploads/2019/10/icon-design-300x300.png"
                              alt=""
                              width="150"
                              height="150"
                              style="display: block;"
                          />
                      </center>
                  </td>
                  <td id="contentwidth">
                      <strong>Elegant Design</strong><br />
                      Start Go S1 memiliki tampilan yang sangat elegan untuk penampilan kamu, selain itu Start Go S1 sudah dilengkapi dengan Gorilla Glass dan casing yang terbuat dari stainless steel.
                  </td>
              </tr>
              <tr>
                  <td id="imagewidth">
                      <center class="">
                          <img
                              
                              src="http://startgo-id.com/new/wp-content/uploads/2019/10/health-care-icon-300x300.png"
                              alt=""
                              width="150"
                              height="150"
                              style="display: block;"
                          />
                      </center>
                  </td>
                  <td id="contentwidth">
                      <strong>Womens Health Care</strong><br />
                      Untuk perempuan, smartwatch Start Go S1 juga dapat menghitung siklus menstruasi kamu, dengan mengaktifkan Fitur Women Health Care dari Start Go S1.
                  </td>
              </tr>
              <tr>
                  <td id="imagewidth">
                      <center class="">
                          <img
                              src="http://startgo-id.com/new/wp-content/uploads/2019/10/icon-notif-300x300.png"
                              alt=""
                              width="150"
                              height="150"
                              style="display: block;"
                          />
                      </center>
                  </td>
                  <td id="contentwidth">
                      <strong>Notification Control</strong><br />
                      Semua notifikasi dari hape kamu sekarang bisa langsung masuk ke <em>smartwatch </em>Start Go S1.
                  </td>
              </tr>
              <tr>
                  <td id="imagewidth">
                      <center class="">
                          <img
                              src="http://startgo-id.com/new/wp-content/uploads/2019/10/sport-icon-300x300.png"
                              alt=""
                              width="150"
                              height="150"
                              style="display: block;"
                          />
                      </center>
                  </td>
                  <td id="contentwidth">
                      <strong>Sports Mode</strong><br />
                      Pantau terus hidup sehat kamu sehari-hari!<br />
                      Start Go S1 bisa menghitung langkah kaki, kalori, dan jarak tempuh kamu dengan akurat.
                  </td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>