<!DOCTYPE html>
<html>
        <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
          Start Go – Spirit
        </title>
        <link rel="stylesheet" id="customizr-main-css" href="core/style.css" type="text/css" media="all">
        <link rel="stylesheet" id="customizr-main-css" href="core/custom.css" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .tc-header.border-top {
                border-top-color: #009ee3
            }
            .tc-header.border-top {
                border-top-width: 5px;
                border-top-style: solid
            }
        </style>
        <script type="text/javascript" src="core/jquery.js"></script>
        <script type="text/javascript">
            var CZRParams = {
                "assetsPath": "http:\/\/startgo-id.com\/new\/wp-content\/themes\/customizr\/assets\/front\/",
                "menuStickyUserSettings": {
                    "desktop": "stick_up",
                    "mobile": "stick_up"
                },
            
            };

        </script>
        <script type="text/javascript" src="core/tc-scripts.min.js"></script>
        <style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Roboto:ital, wght@0, 100;0, 300;0, 400;0, 500;0, 700;0, 900;1, 100;1, 300;1, 400;1, 500;1, 700;1, 900 &display=swap');
            .container {
                margin-top: 0 !important;
            }
            .textwidget p {
                margin: 0 !important;
                padding: 0 !important;
            }
            .aligncenter {
                margin: 0;
            }
        </style>
 </head>

    <body class="czr-link-hover-underline header-skin-dark sn-right ">

        <div id="tc-sn" class="tc-sn " >
            <nav class="tc-sn side-nav__nav">
                <div class="tc-sn-inner">
                    <div class="hamburger-toggler__container ">
                        <button class="ham-toggler-menu czr-collapsed" data-toggle="sidenav" aria-expanded="false">
                            <span class="ham__toggler-span-wrapper">
                                <span class="line line-1"></span>
                                <span class="line line-2"></span>
                                <span class="line line-3"></span>
                            </span>
                            <span class="screen-reader-text">Menu</span>
                        </button>
                    </div>
                    <div class="nav__menu-wrapper side-nav__menu-wrapper" >
                      <ul id="main-menu" class="side-nav__menu vertical-nav nav__menu">
                        <li class="menu-item">
                            <a href="index.php" class="nav__link">
                                <span class="nav__title">Home</span>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="start.php" class="nav__link">
                                <span class="nav__title">Start Go S1</span>
                            </a>
                        </li>
                        <li  class="menu-item">
                            <a href="#" class="nav__link">
                                <span class="nav__title">Contact Us</span>
                            </a>
                        </li>
                      </ul>
                    </div>
                </div>
                <!-- /.tc-sn-inner  -->
            </nav>
        </div>

        <div id="tc-page-wrap">

            <header class="tc-header  border-top ">
                <div class="primary-navbar__wrapper d-none d-lg-block sticky-visible">
                    <div class="container-fluid" style="width:97%;">
                        <div class="row align-items-center flex-row primary-navbar__row">
                          <div class="branding align-items-center flex-column ">
                            <div class="branding-row d-flex align-self-start flex-row align-items-center">
                                <div class="navbar-brand col-auto ">
                                  <img src="./core/cropped-logo-@2x.png"  style="max-width:250px;max-height:100px">
                                </div>
                            </div>
                          </div>
                            <div class="primary-nav__container flex-lg-column">
                                <div class="primary-nav__wrapper justify-content-end">
                                  <ul class="nav utils flex-row flex-nowrap regular-nav">
                                    <li class="hamburger-toggler__container ">
                                        <button class="ham-toggler-menu czr-collapsed" data-toggle="sidenav" aria-expanded="false">
                                            <span class="ham__toggler-span-wrapper">
                                                <span class="line line-1"></span>
                                                <span class="line line-2"></span>
                                                <span class="line line-3"></span>
                                            </span>
                                            <span class="screen-reader-text">Menu</span>
                                        </button>
                                    </li>
                                  </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-navbar__wrapper d-lg-none mobile-sticky">
                    <div class="branding__container justify-content-between align-items-center container-fluid">
                        <div class="branding flex-column">
                            <div class="branding-row d-flex align-self-start flex-row align-items-center">
                                <div class="navbar-brand col-auto ">
                                        <img src="./core/cropped-logo-@2x.png" style="max-width:250px;max-height:100px">
                                </div>
                            </div>
                        </div>
                        <div class="mobile-utils__wrapper nav__utils regular-nav">
                            <ul class="nav utils row flex-row flex-nowrap">
                                <li class="nav__search ">
                                    <a href="http://startgo-id.com/new/#" class="search-toggle_btn  czr-dropdown" data-aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-search"></i>
                                    </a>
                                    <ul class="dropdown-menu czr-dropdown-menu">
                                        <li class="header-search__container container-fluid">
                                            <div class="search-form__container ">
                                                <form action="http://startgo-id.com/new/" method="get" class="czr-form search-form">
                                                    <div class="form-group czr-focus">
                                                        <label for="s-5ee50bd077d3d" id="lsearch-5ee50bd077d3d">
                                                            <span>Search</span>
                                                            <i class="fa fa-search"></i>
                                                            <i class="fa fa-close"></i>
                                                        </label>
                                                        <input id="s-5ee50bd077d3d" class="form-control czr-search-field czr-focusable" name="s" type="text" value="" aria-describedby="lsearch-5ee50bd077d3d" title="Search …">
                                                    </div>
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hamburger-toggler__container ">
                                    <button class="ham-toggler-menu czr-collapsed" data-toggle="czr-collapse" data-target="#mobile-nav">
                                        <span class="ham__toggler-span-wrapper">
                                            <span class="line line-1"></span>
                                            <span class="line line-2"></span>
                                            <span class="line line-3"></span>
                                        </span>
                                        <span class="screen-reader-text">Menu</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-nav__container ">
                        <nav class="mobile-nav__nav flex-column czr-collapse" id="mobile-nav">
                            <div class="mobile-nav__inner container-fluid">
                                <div class="nav__menu-wrapper mobile-nav__menu-wrapper czr-open-on-click">
                                    <ul id="mobile-nav-menu" class="mobile-nav__menu vertical-nav nav__menu flex-column nav">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49">
                                            <a href="http://startgo-id.com/new/?page_id=2" class="nav__link">
                                                <span class="nav__title">Home</span>
                                            </a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50">
                                            <a href="http://startgo-id.com/new/start-go-s1-id/" class="nav__link">
                                                <span class="nav__title">Start Go S1</span>
                                            </a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51">
                                            <a href="http://startgo-id.com/new/contact-us-id/" class="nav__link">
                                                <span class="nav__title">Contact Us</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>


        </div>